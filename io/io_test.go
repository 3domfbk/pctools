package pc

import (
	"fmt"
	"log"
	"testing"

	"bitbucket.org/3domfbk/commontools/geom"
	pc "bitbucket.org/3domfbk/pctools/types"
	. "github.com/smartystreets/goconvey/convey"
)

// A test file
type test struct {
	file, desc string
	expected   pc.PointCloud
}

// Test point cloud reading
func TestReadPly(t *testing.T) {
	cloud1 := pc.PointCloud{
		Points: []pc.Point{
			{Pos: geom.Vec3{X: 1.887784, Y: 0.53859299, Z: -1.817439}, R: 23, G: 54, B: 139, Nor: geom.Vec3{X: 0.1238, Y: 0.025, Z: -0.99199998}},
			{Pos: geom.Vec3{X: 1.802472, Y: 0.493312, Z: -1.829227}, R: 0, G: 1, B: 2, Nor: geom.Vec3{X: 0.1238, Y: 0.025, Z: -0.99199998}},
			{Pos: geom.Vec3{X: 1.860947, Y: 0.56032401, Z: -1.820242}, R: 23, G: 59, B: 234, Nor: geom.Vec3{X: 0.1238, Y: 0.025, Z: -0.99199998}},
			{Pos: geom.Vec3{X: 1.8265361, Y: 0.63863099, Z: -1.806807}, R: 255, G: 255, B: 255, Nor: geom.Vec3{X: -0.0308, Y: -0.0044, Z: -0.99949998}},
			{Pos: geom.Vec3{X: 1.881238, Y: 0.71782601, Z: -1.808843}, R: 254, G: 253, B: 252, Nor: geom.Vec3{X: -0.0308, Y: -0.0044, Z: -0.99949998}},
			{Pos: geom.Vec3{X: 1.866944, Y: 0.649441, Z: -1.8081011}, R: 1, G: 4, B: 59, Nor: geom.Vec3{X: -0.0308, Y: -0.0044, Z: -0.99949998}},
			{Pos: geom.Vec3{X: 1.90747, Y: 0.40673, Z: -1.786278}, R: 34, G: 23, B: 84, Nor: geom.Vec3{X: 0.52029997, Y: -0.1221, Z: -0.8452}},
			{Pos: geom.Vec3{X: 1.855412, Y: 0.43359801, Z: -1.822202}, R: 13, G: 24, B: 13, Nor: geom.Vec3{X: 0.52029997, Y: -0.1221, Z: -0.8452}},
			{Pos: geom.Vec3{X: 1.887784, Y: 0.53859299, Z: -1.817439}, R: 1, G: 1, B: 1, Nor: geom.Vec3{X: 0.52029997, Y: -0.1221, Z: -0.8452}},
			{Pos: geom.Vec3{X: 1.773631, Y: 0.462679, Z: -1.8140351}, R: 1, G: 0, B: 13, Nor: geom.Vec3{X: -0.2852, Y: -0.5898, Z: -0.75559998}},
		},
		HasColors:  true,
		HasNormals: true,
	}

	cloud2 := pc.PointCloud{
		Points: []pc.Point{
			{Pos: geom.Vec3{X: 1.887784, Y: 0.53859299, Z: -1.817439}, R: 23, G: 54, B: 139, Nor: geom.Vec3{X: 0.1238, Y: 0.025, Z: -0.99199998}},
			{Pos: geom.Vec3{X: 1.802472, Y: 0.493312, Z: -1.829227}, R: 0, G: 1, B: 2, Nor: geom.Vec3{X: 0.1238, Y: 0.025, Z: -0.99199998}},
			{Pos: geom.Vec3{X: 1.860947, Y: 0.56032401, Z: -1.820242}, R: 23, G: 59, B: 234, Nor: geom.Vec3{X: 0.1238, Y: 0.025, Z: -0.99199998}},
			{Pos: geom.Vec3{X: 1.8265361, Y: 0.63863099, Z: -1.806807}, R: 255, G: 255, B: 255, Nor: geom.Vec3{X: -0.0308, Y: -0.0044, Z: -0.99949998}},
			{Pos: geom.Vec3{X: 1.881238, Y: 0.71782601, Z: -1.808843}, R: 254, G: 253, B: 252, Nor: geom.Vec3{X: -0.0308, Y: -0.0044, Z: -0.99949998}},
			{Pos: geom.Vec3{X: 1.866944, Y: 0.649441, Z: -1.8081011}, R: 1, G: 4, B: 59, Nor: geom.Vec3{X: -0.0308, Y: -0.0044, Z: -0.99949998}},
			{Pos: geom.Vec3{X: 1.90747, Y: 0.40673, Z: -1.786278}, R: 34, G: 23, B: 84, Nor: geom.Vec3{X: 0.52029997, Y: -0.1221, Z: -0.8452}},
			{Pos: geom.Vec3{X: 1.855412, Y: 0.43359801, Z: -1.822202}, R: 13, G: 24, B: 13, Nor: geom.Vec3{X: 0.52029997, Y: -0.1221, Z: -0.8452}},
			{Pos: geom.Vec3{X: 1.887784, Y: 0.53859299, Z: -1.817439}, R: 1, G: 1, B: 1, Nor: geom.Vec3{X: 0.52029997, Y: -0.1221, Z: -0.8452}},
			{Pos: geom.Vec3{X: 1.773631, Y: 0.462679, Z: -1.8140351}, R: 1, G: 0, B: 13, Nor: geom.Vec3{X: -0.2852, Y: -0.5898, Z: -0.75559998}},
		},
		HasColors:  true,
		HasNormals: true,
		Attributes: make(map[string]pc.PointsAttribute),
	}

	classification1 := []uint8{3, 4, 4, 4, 4, 2, 2, 2, 1, 4}

	testFiles := []test{
		{"testdata/cloud-ascii.ply", "ascii ply file", cloud1},
		{"testdata/cloud-ascii-moreprops.ply", "ascii ply file with unknown properties", cloud1},
		{"testdata/cloud-ascii-normals.ply", "ascii ply file with normals", cloud1},
		{"testdata/cloud-ascii-rgb.ply", "ascii ply file with rgb", cloud1},
		{"testdata/cloud-binary.ply", "binary ply file", cloud1},
		{"testdata/cloud-binary-moreprops.ply", "binary ply file with unknown properties", cloud1},
		{"testdata/cloud-binary-normals.ply", "binary ply file with normals", cloud1},
		{"testdata/cloud-binary-rgb.ply", "binary ply file with rgb", cloud1},
	}

	for _, test := range testFiles {
		Convey("Given an "+test.desc, t, func() {
			pc, err := ReadPc(test.file)
			Convey("It should correctly parse it as a point cloud", func() {
				So(err, ShouldBeNil)
				So(pc.Points, ShouldHaveLength, len(test.expected.Points))
				for i := 0; i < len(pc.Points); i++ {
					// Points
					So(pc.Points[i].Pos.X, ShouldAlmostEqual, test.expected.Points[i].Pos.X, .01)
					So(pc.Points[i].Pos.Y, ShouldAlmostEqual, test.expected.Points[i].Pos.Y, .01)
					So(pc.Points[i].Pos.Z, ShouldAlmostEqual, test.expected.Points[i].Pos.Z, .01)

					// Normals
					if pc.HasNormals {
						So(pc.Points[i].Nor.X, ShouldAlmostEqual, test.expected.Points[i].Nor.X, .01)
						So(pc.Points[i].Nor.Y, ShouldAlmostEqual, test.expected.Points[i].Nor.Y, .01)
						So(pc.Points[i].Nor.Z, ShouldAlmostEqual, test.expected.Points[i].Nor.Z, .01)
					}

					// Colors
					if pc.HasColors {
						So(pc.Points[i].R, ShouldEqual, test.expected.Points[i].R)
						So(pc.Points[i].G, ShouldEqual, test.expected.Points[i].G)
						So(pc.Points[i].B, ShouldEqual, test.expected.Points[i].B)
					}
				}
			})
		})

		Convey("Given a point cloud", t, func() {
			Convey("It should correctly write it to a .ply", func() {
				err := WritePc("testdata/out1.ply", &cloud1)
				So(err, ShouldBeNil)
			})

			Convey("It should correctly parse it back", func() {
				pc, err := ReadPc("testdata/out1.ply")
				So(pc, ShouldNotBeNil)
				So(err, ShouldBeNil)

				So(pc.Points, ShouldHaveLength, len(cloud1.Points))
				for i := 0; i < len(pc.Points); i++ {
					// Points
					So(pc.Points[i].Pos.X, ShouldAlmostEqual, cloud1.Points[i].Pos.X, .01)
					So(pc.Points[i].Pos.Y, ShouldAlmostEqual, cloud1.Points[i].Pos.Y, .01)
					So(pc.Points[i].Pos.Z, ShouldAlmostEqual, cloud1.Points[i].Pos.Z, .01)

					// Normals
					if pc.HasNormals {
						So(pc.Points[i].Nor.X, ShouldAlmostEqual, cloud1.Points[i].Nor.X, .01)
						So(pc.Points[i].Nor.Y, ShouldAlmostEqual, cloud1.Points[i].Nor.Y, .01)
						So(pc.Points[i].Nor.Z, ShouldAlmostEqual, cloud1.Points[i].Nor.Z, .01)
					}

					// Colors
					if pc.HasColors {
						So(pc.Points[i].R, ShouldEqual, cloud1.Points[i].R)
						So(pc.Points[i].G, ShouldEqual, cloud1.Points[i].G)
						So(pc.Points[i].B, ShouldEqual, cloud1.Points[i].B)
					}
				}
			})
		})

		Convey("Given a classified point cloud", t, func() {
			Convey("It should correctly write it to a .ply", func() {
				c := make([]interface{}, len(classification1))
				for i := 0; i < len(classification1); i++ {
					c[i] = classification1[i]
				}
				cloud2.Attributes["class"] = pc.PointsAttribute{DataType: pc.TUChar, Data: c}
				err := WritePc("testdata/out2.ply", &cloud2)
				So(err, ShouldBeNil)
			})

			Convey("It should correctly parse it back", func() {
				pc, err := ReadPc("testdata/out2.ply")
				So(pc, ShouldNotBeNil)
				So(err, ShouldBeNil)

				test := make([]interface{}, len(classification1))
				for i := 0; i < len(classification1); i++ {
					test[i] = classification1[i]
				}
				So(pc.Attributes["class"].Data, ShouldResemble, test)
			})
		})
	}
}

func BenchmarkReadPly(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ReadPc("testdata/cittalta.ply")
	}
}

// Example reads a ply point cloud and displays its number of points
func Example() {
	pc, err := ReadPc("testdata/cloud-ascii.ply")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("Points: %d", len(pc.Points))
	}
	// Output: Points: 10
}
