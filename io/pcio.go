package pc

import (
	"fmt"
	"path/filepath"

	pc "bitbucket.org/3domfbk/pctools/types"
)

// ReadPc reads a point cloud from the specified file
func ReadPc(filename string) (*pc.PointCloud, error) {
	extension := filepath.Ext(filename)
	switch extension {
	case ".ply":
		return readPly(filename)
	}

	return nil, fmt.Errorf("Format not supported: %s", extension)
}

// WritePc writes a point cloud to the specified file.
// The extension determines the output format
func WritePc(filename string, pcloud *pc.PointCloud) error {
	extension := filepath.Ext(filename)
	switch extension {
	case ".ply":
		return writePly(filename, pcloud)
	}

	return fmt.Errorf("Format not supported: %s", extension)
}
