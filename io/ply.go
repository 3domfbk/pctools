package pc

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"math"
	"os"

	"strings"

	"strconv"

	"bitbucket.org/3domfbk/commontools/geom"
	pc "bitbucket.org/3domfbk/pctools/types"
)

// Ply
const (
	LitDouble = "double"
	LitFloat  = "float"
	LitInt    = "int"
	LitUInt   = "uint"
	LitChar   = "char"
	LitUChar  = "uchar"
	LitByte   = "byte"

	LitR            = "r"
	LitRed          = "red"
	LitDiffuseRed   = "diffuse_red"
	LitG            = "g"
	LitGreen        = "green"
	LitDiffuseGreen = "diffuse_green"
	LitB            = "b"
	LitBlue         = "blue"
	LitDiffuseBlue  = "diffuse_blue"
)

const (
	pX = iota
	pY
	pZ
	pNX
	pNY
	pNZ
	pR
	pG
	pB
	pCustom
)

// Ply element
type element struct {
	prop, proptype int
	name           string
}

// ReadPly reads a point cloud from a .ply file
func readPly(filename string) (*pc.PointCloud, error) {
	// Open file
	f, err := os.Open(filename)
	defer f.Close()

	if err != nil {
		return nil, err
	}

	// Read header
	readingVertices := false
	var numVertices int64
	elements := make([]element, 0, 10)
	ascii := true
	headerBytes := 0
	hasNormals, hasColors := false, false
	var endianity binary.ByteOrder

	reader := bufio.NewReader(f)
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()
		headerBytes += len(line) + 1
		values := strings.Fields(line)
		endHeader := false

		switch values[0] {
		case "end_header":
			endHeader = true
		case "format":
			switch values[1] {
			case "binary_little_endian":
				ascii = false
				endianity = binary.LittleEndian
			case "binary_big_endian":
				ascii = false
				endianity = binary.BigEndian
			}
		case "element":
			if values[1] == "vertex" {
				readingVertices = true
				numVertices, err = strconv.ParseInt(values[2], 10, 64)
				if err != nil {
					return nil, err
				}
			} else {
				readingVertices = false
			}

		case "property":
			if !readingVertices {
				continue
			}

			var t, p int
			switch values[2] {
			case "x":
				p = pX
			case "y":
				p = pY
			case "z":
				p = pZ
			case "nx":
				p = pNX
				hasNormals = true
			case "ny":
				p = pNY
			case "nz":
				p = pNZ
			case LitR, LitRed, LitDiffuseRed:
				p = pR
				hasColors = true
			case LitG, LitGreen, LitDiffuseGreen:
				p = pG
			case LitB, LitBlue, LitDiffuseBlue:
				p = pB
			default:
				p = pCustom
			}

			switch values[1] {
			case LitDouble:
				t = pc.TDouble
			case LitFloat:
				t = pc.TFloat
			case LitInt:
				t = pc.TInt
			case LitUInt:
				t = pc.TUInt
			case LitChar:
				t = pc.TChar
			case LitUChar:
				t = pc.TUChar
			case LitByte:
				t = pc.TByte
			default:
				return nil, fmt.Errorf("Unkown property type: %s", values[1])
			}
			elements = append(elements, element{name: values[2], prop: p, proptype: t})
		}

		// Header end
		if endHeader {
			break
		}
	}

	// Create point cloud
	pcloud := pc.NewPointCloud(numVertices, hasNormals, hasColors)

	// Add custom Attributes
	for _, e := range elements {
		if e.prop == pCustom {
			pcloud.Attributes[e.name] = pc.PointsAttribute{DataType: e.proptype, Data: make([]interface{}, 0, numVertices)}
		}
	}

	if ascii {
		err = readPlyASCII(pcloud, scanner, numVertices, elements)
	} else {
		f.Close()
		f, err = os.Open(filename)
		if err != nil {
			return nil, err
		}
		reader = bufio.NewReader(f)
		reader.Discard(headerBytes)
		err = readPlyBinary(pcloud, reader, numVertices, endianity, elements)
	}

	return pcloud, err
}

// Read ascii ply
func readPlyASCII(pcloud *pc.PointCloud, scanner *bufio.Scanner, numVertices int64, elements []element) error {
	var err error

	// Read vertices
	for scanner.Scan() {
		var p pc.Point
		p.Nor = geom.Vec3{X: -2, Y: -2, Z: -2}
		values := strings.Fields(scanner.Text())

		for i := 0; i < len(elements); i++ {
			e := elements[i]
			switch e.name {
			case "x":
				p.Pos.X, err = strconv.ParseFloat(values[i], 64)
			case "y":
				p.Pos.Y, err = strconv.ParseFloat(values[i], 64)
			case "z":
				p.Pos.Z, err = strconv.ParseFloat(values[i], 64)
			case "nx":
				p.Nor.X, err = strconv.ParseFloat(values[i], 64)
			case "ny":
				p.Nor.Y, err = strconv.ParseFloat(values[i], 64)
			case "nz":
				p.Nor.Z, err = strconv.ParseFloat(values[i], 64)
			case LitR, LitRed, LitDiffuseRed:
				var v uint64
				v, err = strconv.ParseUint(values[i], 10, 8)
				if err == nil {
					p.R = uint8(v)
				}
			case LitG, LitGreen, LitDiffuseGreen:
				var v uint64
				v, err = strconv.ParseUint(values[i], 10, 8)
				if err == nil {
					p.G = uint8(v)
				}
			case LitB, LitBlue, LitDiffuseBlue:
				var v uint64
				v, err = strconv.ParseUint(values[i], 10, 8)
				if err == nil {
					p.B = uint8(v)
				}

			default:
				// Lookup for custom prop
				prop, ok := pcloud.Attributes[e.name]
				if ok {
					switch prop.DataType {
					case pc.TDouble, pc.TFloat:
						var v float64
						v, err = strconv.ParseFloat(values[i], 64)
						if err == nil {
							prop.Data = append(prop.Data, v)
						}

					case pc.TInt, pc.TUInt:
						var v uint64
						v, err = strconv.ParseUint(values[i], 10, 8)
						if err == nil {
							prop.Data = append(prop.Data, int(v))
						}

					case pc.TChar, pc.TUChar, pc.TByte:
						var v uint64
						v, err = strconv.ParseUint(values[i], 10, 8)
						if err == nil {
							prop.Data = append(prop.Data, uint8(v))
						}
					}

					pcloud.Attributes[e.name] = prop
				}
			}
		}

		// Number format error
		if err != nil {
			return err
		}

		pcloud.AddPoint(p)

		// Don't read too much
		if int64(len(pcloud.Points)) == numVertices {
			break
		}
	}

	return err
}

// Read binary ply
func readPlyBinary(pcloud *pc.PointCloud, reader *bufio.Reader, numVertices int64, endianity binary.ByteOrder, elements []element) error {
	var err error

	// Some buffers
	eight := make([]byte, 8)
	four := make([]byte, 4)

	for pt := int64(0); pt < numVertices; pt++ {
		var p pc.Point
		p.Nor = geom.Vec3{X: -2, Y: -2, Z: -2}

		for i := 0; i < len(elements); i++ {
			e := elements[i]
			switch e.prop {
			case pX:
				switch e.proptype {
				case pc.TDouble:
					reader.Read(eight)
					p.Pos.X = math.Float64frombits(endianity.Uint64(eight))
				case pc.TFloat:
					reader.Read(four)
					p.Pos.X = float64(math.Float32frombits(endianity.Uint32(four)))
				}
			case pY:
				switch e.proptype {
				case pc.TDouble:
					reader.Read(eight)
					p.Pos.Y = math.Float64frombits(endianity.Uint64(eight))
				case pc.TFloat:
					reader.Read(four)
					p.Pos.Y = float64(math.Float32frombits(endianity.Uint32(four)))
				}
			case pZ:
				switch e.proptype {
				case pc.TDouble:
					reader.Read(eight)
					p.Pos.Z = math.Float64frombits(endianity.Uint64(eight))
				case pc.TFloat:
					reader.Read(four)
					p.Pos.Z = float64(math.Float32frombits(endianity.Uint32(four)))
				}

			case pNX:
				switch e.proptype {
				case pc.TDouble:
					reader.Read(eight)
					p.Nor.X = math.Float64frombits(endianity.Uint64(eight))
				case pc.TFloat:
					reader.Read(four)
					p.Nor.X = float64(math.Float32frombits(endianity.Uint32(four)))
				}
			case pNY:
				switch e.proptype {
				case pc.TDouble:
					reader.Read(eight)
					p.Nor.Y = math.Float64frombits(endianity.Uint64(eight))
				case pc.TFloat:
					reader.Read(four)
					p.Nor.Y = float64(math.Float32frombits(endianity.Uint32(four)))
				}
			case pNZ:
				switch e.proptype {
				case pc.TDouble:
					reader.Read(eight)
					p.Nor.Z = math.Float64frombits(endianity.Uint64(eight))
				case pc.TFloat:
					reader.Read(four)
					p.Nor.Z = float64(math.Float32frombits(endianity.Uint32(four)))
				}

			case pR:
				p.R, _ = reader.ReadByte()
			case pG:
				p.G, _ = reader.ReadByte()
			case pB:
				p.B, _ = reader.ReadByte()

			default:
				// Check for custom prop
				// Lookup for custom prop
				prop, ok := pcloud.Attributes[e.name]
				if ok {
					switch prop.DataType {
					case pc.TDouble:
						reader.Read(eight)
						prop.Data = append(prop.Data, math.Float64frombits(endianity.Uint64(eight)))
					case pc.TFloat:
						reader.Read(four)
						prop.Data = append(prop.Data, math.Float32frombits(endianity.Uint32(four)))
					case pc.TUChar, pc.TChar, pc.TByte:
						v, _ := reader.ReadByte()
						prop.Data = append(prop.Data, v)
					case pc.TInt, pc.TUInt:
						reader.Read(four)
						prop.Data = append(prop.Data, endianity.Uint32(four))
					}

					pcloud.Attributes[e.name] = prop
				} else {
					switch e.proptype {
					case pc.TFloat:
						reader.Discard(4)
					case pc.TDouble:
						reader.Discard(8)
					case pc.TUChar, pc.TChar, pc.TByte:
						reader.Discard(1)
					case pc.TInt, pc.TUInt:
						reader.Discard(4)
					}
				}
			}
		}

		pcloud.AddPoint(p)
	}
	return err
}

func writePly(filename string, pcloud *pc.PointCloud) error {
	// Open file
	f, err := os.Create(filename)
	defer f.Close()
	if err != nil {
		return err
	}

	writer := bufio.NewWriter(f)

	// Write header
	writer.WriteString("ply\n")
	writer.WriteString("format binary_little_endian 1.0\n")
	writer.WriteString("element vertex " + strconv.Itoa(len(pcloud.Points)) + "\n")
	writer.WriteString("property double x\n")
	writer.WriteString("property double y\n")
	writer.WriteString("property double z\n")

	if pcloud.HasNormals {
		writer.WriteString("property float nx\n")
		writer.WriteString("property float ny\n")
		writer.WriteString("property float nz\n")
	}

	if pcloud.HasColors {
		writer.WriteString("property uchar red\n")
		writer.WriteString("property uchar green\n")
		writer.WriteString("property uchar blue\n")
	}

	// Write remaining specs
	for k, v := range pcloud.Attributes {
		var dataTypeString string
		switch v.DataType {
		case pc.TDouble:
			dataTypeString = "double"
		case pc.TFloat:
			dataTypeString = "float"
		case pc.TInt:
			dataTypeString = "int"
		case pc.TUInt:
			dataTypeString = "uint"
		case pc.TChar:
			dataTypeString = "char"
		case pc.TUChar:
			dataTypeString = "uchar"
		case pc.TByte:
			dataTypeString = "byte"
		}
		writer.WriteString("property " + dataTypeString + " " + k + "\n")
	}

	writer.WriteString("end_header\n")

	// Write the binary!
	eight := make([]byte, 8)
	four := make([]byte, 4)
	order := binary.LittleEndian

	for i, p := range pcloud.Points {
		order.PutUint64(eight, math.Float64bits(p.Pos.X))
		writer.Write(eight)
		order.PutUint64(eight, math.Float64bits(p.Pos.Y))
		writer.Write(eight)
		order.PutUint64(eight, math.Float64bits(p.Pos.Z))
		writer.Write(eight)

		if pcloud.HasNormals {
			order.PutUint32(four, math.Float32bits(float32(p.Nor.X)))
			writer.Write(four)
			order.PutUint32(four, math.Float32bits(float32(p.Nor.Y)))
			writer.Write(four)
			order.PutUint32(four, math.Float32bits(float32(p.Nor.Z)))
			writer.Write(four)
		}

		if pcloud.HasColors {
			writer.WriteByte(p.R)
			writer.WriteByte(p.G)
			writer.WriteByte(p.B)
		}

		// Attributes
		for _, v := range pcloud.Attributes {
			switch v.DataType {
			case pc.TByte, pc.TChar, pc.TUChar:
				writer.WriteByte(v.Data[i].(byte))
			case pc.TInt, pc.TUInt:
				order.PutUint32(four, v.Data[i].(uint32))
				writer.Write(four)
			case pc.TFloat:
				order.PutUint32(four, math.Float32bits(v.Data[i].(float32)))
				writer.Write(four)
			case pc.TDouble:
				order.PutUint64(eight, math.Float64bits(v.Data[i].(float64)))
				writer.Write(eight)
			}
		}
	}

	writer.Flush()
	return nil
}
