package pc

import "bitbucket.org/3domfbk/commontools/geom"

// Attribute Types
const (
	TDouble = iota
	TFloat
	TInt
	TUInt
	TChar
	TUChar
	TByte
)

// Point of a point cloud
type Point struct {
	Pos     geom.Vec3
	Nor     geom.Vec3
	R, G, B uint8
}

// PointsAttribute is an arbitrary attribute
type PointsAttribute struct {
	DataType int
	Data     []interface{}
}

// PointCloud is a set of points
type PointCloud struct {
	Points                []Point
	HasNormals, HasColors bool
	Attributes            map[string]PointsAttribute
}

// NewPointCloud creates a fixed-size point cloud
func NewPointCloud(points int64, hasNormals, hasColors bool) *PointCloud {
	return &PointCloud{make([]Point, 0, points), hasNormals, hasColors, make(map[string]PointsAttribute)}
}

// AddPoint to the cloud
func (pc *PointCloud) AddPoint(p Point) {
	pc.Points = append(pc.Points, p)
}
